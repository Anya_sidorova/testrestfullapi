<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $query = Product::query();

        $product = new Product();

        if ($request->has('name')) {
            $query = $product->scopeName($request, $query);
        }

        if ($request->has('categories_id')) {
            $query = $product->scopeCategoriesId($request, $query);
        }

        if ($request->has('categories_name')) {
            $query = $product->scopeCategoriesName($request, $query);
        }

        if ($request->has('published')) {
            $query = $product->scopePublished($request, $query);
        }

        if ($request->has('min_price')) {
            $query = $product->scopeMinPrice($request, $query);
        }

        if ($request->has('max_price')) {
            $query = $product->scopeMaxPrice($request, $query);
        }

        $products = $query->with('categories')->get();

        return response()->json($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest $request
     * @return JsonResponse
     */
    public function store(ProductRequest $request): JsonResponse
    {
        $data = $request->all();

        $product = new Product();
        $product->fill($data);
        $product->save();

        $product->categories()->sync($data['categories']);

        $product = Product::with('categories')
            ->where('id', $product->id)->firstOrFail();

        return response()->json([$product], 201);
    }

    /**
     * Display the specified resource.
     *
     * @mixin Builder
     * @param Product $product
     * @return JsonResponse
     */
    public function show(Product $product): JsonResponse
    {
        $product = Product::with('categories')
            ->where('id', $product->id)->firstOrFail();

        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductRequest $request
     * @param Product $product
     * @return JsonResponse
     */
    public function update(ProductRequest $request, Product $product): JsonResponse
    {
        $data = $request->all();
        $product->fill($data);

        $product->categories()->sync($data['categories']);

        $product->save();

        return response()->json($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return Response
     */
    public function destroy(Product $product): Response
    {
        $product->delete();

        return response()->noContent();
    }
}
