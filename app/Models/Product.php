<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $price
 * @property boolean $published
 * @property DateTime $created_at
 * @property DateTime $updated_at
 * @property DateTime $deleted_at
 *
 * @property Collection|null $categories
 */
class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'price',
        'published',
    ];

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @param $request
     * @param $query
     * @return mixed
     */
    public function scopeCategoriesId($request, $query)
    {
        return $query->whereHas('categories', function ($query) use ($request) {
            $query->where('categories.id', $request->categories_id);
        });
    }

    /**
     * @param $request
     * @param $query
     * @return mixed
     */
    public function scopeName($request, $query)
    {
        return $query->where('name', $request->name);
    }

    /**
     * @param $request
     * @param $query
     * @return mixed
     */
    public function scopeCategoriesName($request, $query)
    {
        return $query->whereHas('categories', function ($query) use ($request) {
            $query->where('categories.name', 'like', "%{$request->categories_name}%");
        });
    }

    /**
     * @param $request
     * @param $query
     * @return mixed
     */
    public function scopePublished($request, $query)
    {
        return $query->where('published', $request->published);
    }

    /**
     * @param $request
     * @param $query
     * @return mixed
     */
    public function scopeMinPrice($request, $query)
    {
        return $query->where('price', '>=', $request->min_price);
    }

    /**
     * @param $request
     * @param $query
     * @return mixed
     */
    public function scopeMaxPrice($request, $query)
    {
        return $query->where('price', '<=', $request->max_price);
    }
}
